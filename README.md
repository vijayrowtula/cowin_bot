## cowin_bot - vijay rowtula
## date 01-Jun-2021

About - jupyter notebook for simple polling and sound alert using Co-WIN Public APIs from https://apisetu.gov.in/public/marketplace/api/cowin

package needed:
1. pip install pydub

* The poller looks for dose 1 (see code comments).
* Code alerts user only if number of slots available are more than 1 (probably a cancellation slot).
* poller skips alerts of does from same hospitals for next 5 minutes. Info is maintained as API buffer for 5 minutes (not 30!)
* Notebook --> run all cells. It will run continuously, alerting when slots are available.
